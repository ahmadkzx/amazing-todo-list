const fs = require('fs')
const path = require('path')

const TEMPLATE = {
  "todo-list": [
    {
      "title": "Task 1",
      "status": "TODO",
      "id": 1
    }
  ]
}

function prepareDatabase() {
  try {
    const dist = path.join(__dirname, '../db.json')
    const isExist = fs.existsSync(dist)

    if (!isExist) fs.writeFileSync(dist, JSON.stringify(TEMPLATE), 'utf-8')

    console.log('✅ Database is ready!')
  } catch(err) {
    console.error(err)
  }
}

prepareDatabase()