import Head from 'next/head'
import Error from 'next/error'
import classes from './index.module.scss'
import { AppContext } from '../contexts/app'
import { useContext, useEffect } from 'react'
import TodoPanel from '../components/TodoPanel'

export default function Home({ error, items }) {
  if (error) {
    return <Error statusCode={error} />
  }
  const [app, dispatch] = useContext(AppContext)

  useEffect(setItemsInContext, [])
  
  function setItemsInContext() {
    dispatch({
      type: 'SET_ITEMS',
      payload: items
    })
  }

  return (
    <div>
      <Head>
        <title>Amazing Todo List</title>
        <meta name="description" content="Developed with love by Ahmad Karimzade" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={classes['home']}>
        <h1 className={classes['home__title']}>Todo</h1>

        <TodoPanel />
      </div>
    </div>
  )
}

export async function getServerSideProps() {
  try {
    const endpoint = process.env.API_URL + '/todo-list'
    var result = await fetch(endpoint)
    const items = await result.json()
  
    return {
      props: {
        items
      }
    }
  } catch(err) {
    console.error(err)
    return {
      props: {
        error: result?.status || 500
      }
    }
  }
}