import '../styles/global.scss'
import '../styles/variables.scss'
import { AppContextProvider } from '../contexts/app'

function MyApp({ Component, pageProps }) {
  return (
    <AppContextProvider>
      <Component {...pageProps} />
    </AppContextProvider>
    )
}

export default MyApp
