# Amazing Todo List
Test Project For SnappShop

## ✨ Dev
```bash
yarn

yarn start
```

## 🕹 Notes
- for show many items in page we should use **Virtual Scroll** but i didnt have time for using that
- json-server dose not have bulk delete so i have to use multiple requests
- if app didnt run please process on port 3000 and 3001