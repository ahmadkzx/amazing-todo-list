import classes from './index.module.scss'
import { useContext, useState } from 'react'
import { AppContext } from '../../contexts/app'

export default function TodoItem({ id, title, status }) {
  const [app, dispatch] = useContext(AppContext)
  const [isEditing, setIsEditing] = useState(false)
  const [editedTitle, setEditedTitle] = useState(title)

  async function saveItem(editedStatus) {
    try {
      const endpoint = process.env.API_URL + '/todo-list/' + id
      const body = {
        title: editedTitle,
        status: (editedStatus ?? status) ? 'DONE' : 'TODO'
      }
      await fetch(endpoint, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
      })

      dispatch({
        type: 'EDIT_ITEM',
        payload: {
          id,
          title: editedTitle,
          status: (editedStatus ?? status) ? 'DONE' : 'TODO'
        }
      })
    } catch(err) {
      console.error(err)
    }
  }

  async function deleteItem() {
    try {
      const endpoint = process.env.API_URL + '/todo-list/' + id
      await fetch(endpoint, {
        method: 'DELETE',
      })

      dispatch({
        type: 'DELETE_ITEM',
        payload: id
      })
    } catch(err) {
      console.error(err)
    }
  }

  function handleKeyPress(e) {
    if (e.code == 'Enter') saveItem()
  }

  return (
    <li className={classes["todo-item"]} id={'todo-item-' + id} data-test-id="todo-item">
      <input
        className={classes["todo-item__checkbox"]}
        data-test-id="todo-item-checkbox"
        type="checkbox"
        checked={status == 'DONE'}
        onChange={e => saveItem(e.target.checked)}
      />

      <input
        className={classes["todo-item__title"]}
        data-test-id="todo-item-title"
        value={isEditing ? editedTitle : title}
        readOnly={!isEditing}
        onKeyDown={handleKeyPress}
        onDoubleClick={() => setIsEditing(true)}
        onChange={e => setEditedTitle(e.target.value)}
      />

      <button className={classes["todo-item__close"]} data-test-id="todo-item-delete" onClick={deleteItem}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="12"
          height="12"
          viewBox="0 0 121.31 122.876"
        >
          <path
            fill="var(--brown-light)"
            d="M90.914,5.296c6.927-7.034,18.188-7.065,25.154-0.068 c6.961,6.995,6.991,18.369,0.068,25.397L85.743,61.452l30.425,30.855c6.866,6.978,6.773,18.28-0.208,25.247 c-6.983,6.964-18.21,6.946-25.074-0.031L60.669,86.881L30.395,117.58c-6.927,7.034-18.188,7.065-25.154,0.068 c-6.961-6.995-6.992-18.369-0.068-25.397l30.393-30.827L5.142,30.568c-6.867-6.978-6.773-18.28,0.208-25.247 c6.983-6.963,18.21-6.946,25.074,0.031l30.217,30.643L90.914,5.296L90.914,5.296z"
          />
        </svg>
      </button>
    </li>
  );
}
