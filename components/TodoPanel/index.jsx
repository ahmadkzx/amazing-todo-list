import TodoItem from '../TodoItem'
import TodoInput from '../TodoInput'
import { useRouter } from 'next/router'
import classes from './index.module.scss'
import { AppContext } from '../../contexts/app'
import { useContext, useState, useEffect } from 'react'

export default function TodoPanel() {
  const router = useRouter()
  const [filter, setFilter] = useState('ALL')
  const [app, dispatch] = useContext(AppContext)
  const [doneItems, setDoneItems] = useState([])
  const [activeItems, setActiveItems] = useState([])

  useEffect(() => {
    const routerFilter = router.query.filter
    if (routerFilter == 'ALL' || routerFilter == 'ACTIVE') {
      setFilter(routerFilter)
    }
  }, [])

  useEffect(() => {
    setDoneItems(app?.items.filter(item => item.status == 'DONE') || [])
    setActiveItems(app?.items.filter(item => item.status == 'TODO') || [])
  }, [app?.stamp])

  async function clearDoneItems() {
    try {
      const ids = []
      const promises = []

      doneItems.forEach(item => {
        const endpoint = process.env.API_URL + '/todo-list/' + item.id
        promises.push(fetch(endpoint, { method: 'DELETE' }))
        ids.push(item.id)
      })
      
      await Promise.all(promises)

      dispatch({
        type: 'DELETE_DONE_ITEMS',
        payload: ids
      })
    } catch(err) {
      console.error(err)
    }
  }

  function changeFilter(e) {
    const newFilter = e.target.value
    setFilter(newFilter)
    router.push('?filter=' + newFilter)
  }

  return (
    <div className={classes['todo-panel']}>
      <div className={classes['todo-panel-body']}>
        <TodoInput />

        <ul>
          {
            filter == 'ALL'
              ? app?.items.map(todo => <TodoItem key={'todo-' + todo.id} id={todo.id} title={todo.title} status={todo.status} />)
              : activeItems.map(todo => <TodoItem key={'todo-' + todo.id} id={todo.id} title={todo.title} status={todo.status} />)
          }
        </ul>
      </div>

      <div className={classes['todo-panel-footer']}>
        <div className={classes['todo-panel-footer-section']}>
          <span><b>{activeItems.length}</b> items left</span>
        </div>

        <div className={classes['todo-panel-footer-section']}>
          <button
            className={`${classes['todo-panel-footer-section__filter']} ${filter == 'ALL' && classes['todo-panel-footer-section__filter--active']}`}
            data-test-id="filter-all"
            value="ALL"
            onClick={changeFilter}
          >
            All
          </button>
          <button
            className={`${classes['todo-panel-footer-section__filter']} ${filter == 'ACTIVE' && classes['todo-panel-footer-section__filter--active']}`}
            data-test-id="filter-active"
            value="ACTIVE"
            onClick={changeFilter}
          >
            Active
          </button>
        </div>

        <div className={classes['todo-panel-footer-section']}>
          <button
            className={classes['todo-panel-footer-section__clear']}
            data-test-id="clear-completed"
            disabled={doneItems.length == 0}
            onClick={clearDoneItems}
          >
            Clear completed
          </button>
        </div>
      </div>

      <p className={classes['todo-panel__hint']}>Hint: For save new Todo or edit Todo title press <b>Enter</b> button</p>
    </div>
  )
}