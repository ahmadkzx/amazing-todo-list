import classes from './index.module.scss'
import { useState, useContext } from 'react'
import { AppContext } from '../../contexts/app'

export default function TodoInput() {
  const [title, setTitle] = useState('')
  const [app, dispatch] = useContext(AppContext)

  function handleKeyPress(e) {
    if (e.code == 'Enter') addTodo()
  }

  async function addTodo() {
    try {
      const endpoint = process.env.API_URL + '/todo-list'
      const body = {
        title,
        status: 'TODO'
      }
      const result = await fetch(endpoint, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
      })

      const data = await result.json()

      dispatch({
        type: 'ADD_ITEM',
        payload: data
      })

      setTitle('')
      
    } catch(err) {
      console.error(err)
    }
  }

  return (
    <input
      className={classes['todo-input']}
      type="text"
      placeholder="What needs to be done ?"
      data-test-id="todo-input"
      onKeyDown={handleKeyPress}
      value={title}
      onChange={e => setTitle(e.target.value)}
    />
  )
}