import { createContext, useReducer } from 'react'

const INITIAL_STATE = {
  items: [],
  stamp: Date.now()
}

const AppReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SET_ITEMS':
      return Object.assign({}, state, { items: action.payload, stamp: Date.now() })

    case 'ADD_ITEM':
      var items = [...state.items, action.payload]
      return Object.assign({}, state, { items, stamp: Date.now() })

    case 'EDIT_ITEM':
      var itemIndex = state.items.findIndex(item => item.id == action.payload.id)
      state.items[itemIndex] = action.payload
      return Object.assign({}, state, { stamp: Date.now() })

    case 'DELETE_ITEM':
      var items = state.items.filter(item => item.id != action.payload)
      return Object.assign({}, state, { items, stamp: Date.now() })

    case 'DELETE_DONE_ITEMS':
      var items = state.items.filter(item => !action.payload.includes(item.id))
      return Object.assign({}, state, { items, stamp: Date.now() })

    default:
      return state
  }
}

const AppContext = createContext()

const AppContextProvider = ({ children }) => {
  const [app, dispatch] = useReducer(AppReducer)

  return <AppContext.Provider value={[app, dispatch]}>{children}</AppContext.Provider>
}

export {
  AppContext,
  AppContextProvider
}