/// <reference types="cypress" />

Cypress.Commands.add('getEl', selector => {
  const splitted = selector.split(' ')
  const testId =  splitted[0]
  const restOfSelector = splitted.slice(1).join(' ')

  return cy.get(`[data-test-id="${testId}"] ${restOfSelector}`)
})