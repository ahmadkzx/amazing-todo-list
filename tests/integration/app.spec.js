/// <reference types="cypress" />

describe('Todo List', () => {
  it('Add item', () => {
    cy.visit('/')

    const endpoint = Cypress.env('apiUrl')
    cy.intercept('POST', endpoint, {
      fixture: 'addItem'
    }).as('addItem')

    cy.getEl('todo-input').type('Task 2').type('{enter}')

    cy.wait('@addItem')
    cy.wait(500)

    cy.getEl('todo-item-title').last().should('have.value', 'Task 2')
  })

  it('Delete item', () => {
    const endpoint = Cypress.env('apiUrl') + '/*'
    cy.intercept('DELETE', endpoint, {
      fixture: 'deleteItem'
    }).as('deleteItem')

    cy.getEl('todo-item')
    .first()
    .then($el => {
      const id = $el.attr('id')

      cy.get(`#${id} [data-test-id="todo-item-delete"]`).click()
      cy.wait('@deleteItem')

      cy.get(`#${id}`).should('not.exist')
    })
  })

  it('Edit Item', () => {
    const endpoint = Cypress.env('apiUrl') + '/*'
    cy.intercept('PUT', endpoint, {
      fixture: 'editItem'
    }).as('editItem')

    cy.getEl('todo-item-checkbox').last().check()
    cy.wait('@editItem')
    cy.getEl('todo-item-title').last().dblclick()
    cy.getEl('todo-item-title').last().clear()
    cy.getEl('todo-item-title').last().type('Edited Task 2')
    cy.getEl('todo-item-title').last().type('{enter}')
    cy.wait('@editItem')

    cy.getEl('todo-item-checkbox').last().should('be.checked')
    cy.getEl('todo-item-title').last().should('have.value', 'Edited Task 2')
  })

  it('Filters', () => {
    cy.getEl('filter-active').click()
    cy.getEl('todo-item-checkbox').should('not.to.be.checked')

    cy.getEl('filter-all').click()
  })

  it('Clear Completed', () => {
    const endpoint = Cypress.env('apiUrl') + '/*'
    cy.intercept('DELETE', endpoint, {
      fixture: 'deleteItem'
    }).as('deleteItem')

    cy.wait(1000)

    cy.getEl('clear-completed').click()
    cy.getEl('todo-item-checkbox').should('not.to.be.checked')
  })
})